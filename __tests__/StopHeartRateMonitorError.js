import { HeartRateMonitor, StateCodes } from "..";

import { BleManager, Device } from 'react-native-ble-plx';
jest.mock('react-native-ble-plx');

const DEVICE_ID = 'A3220F29-4A54-4530-B118-C3F0463A747D';
const DEVICE_NAME = 'Mock HR Monitor 2000-x8938-1119220';
const DEVICE_FRIENDLY_NAME = 'Mock HR 2000';

describe('stop heart rate monitor error', () => {
  beforeEach(() => {
    BleManager.mockImplementation( () => {
      return {
        onStateChange: (listener, emitCurrentState) => {
          setTimeout(function() {
            listener('PoweredOn');
          }, 250);
          return ({
            remove: () => ( { } ),
          });
        },
      
        cancelDeviceConnection: (deviceId) => {
          return new Promise((resolve, reject) => {
            reject({statusCode: StateCodes.OTHER, reason: 'general error cancelling device'});
          });
        }

      };
    });
  });  

  it('should fail to stop the heart rate monitor', () => {
    const hrm = new HeartRateMonitor();
    return expect(hrm.stopHeartRateMonitor(DEVICE_ID)).rejects.toMatchObject({error:{statusCode: StateCodes.OTHER, reason: 'general error cancelling device'}});
  })
})