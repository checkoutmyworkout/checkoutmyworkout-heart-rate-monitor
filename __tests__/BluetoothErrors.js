import { HeartRateMonitor, StateCodes } from "..";

import { BleManager } from 'react-native-ble-plx';
jest.mock('react-native-ble-plx');

describe('bluetooth off should be error', () => {
  beforeEach(() => {
    BleManager.mockImplementation( () => {
      return {
        onStateChange: (listener, emitCurrentState) => {
          setTimeout(function() {
            listener('PoweredOff');
          }, 250);
          return ({
            remove: () => ( { } ),
          });
        },
      };
    });
  });  
  
  test('fail with bluetooth off', done => {
    function callback(device, error) {
      expect(device).toBe(null);
      expect(error).toMatchObject({scanCode: StateCodes.POWERED_OFF, reason: 'bluetooth is powered off on device'});
      done();
    }

    const hrm = new HeartRateMonitor();
    hrm.startHeartRateDevicesScan(callback);
  })
})

describe('bluetooth unauthorized should be error', () => {
  beforeEach(() => {
    BleManager.mockImplementation( () => {
      return {
        onStateChange: (listener, emitCurrentState) => {
          setTimeout(function() {
            listener('Unauthorized');
          }, 250);
          return ({
            remove: () => ( { } ),
          });
        },
      };
    });
  });  
  
  test('fail with bluetooth unauthorized', done => {
    function callback(device, error) {
      expect(device).toBe(null);
      expect(error).toMatchObject({scanCode: StateCodes.UNAUTHORIZED, reason: 'user has not allowed application to use bluetooth on this device'});
      done();
    }

    const hrm = new HeartRateMonitor();
    hrm.startHeartRateDevicesScan(callback);
  })
})
