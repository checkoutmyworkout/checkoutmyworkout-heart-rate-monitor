import { HeartRateMonitor, StateCodes } from "..";

import { BleManager, Device } from 'react-native-ble-plx';
jest.mock('react-native-ble-plx');

const DEVICE_ID = 'A3220F29-4A54-4530-B118-C3F0463A747D';
const DEVICE_NAME = 'Mock HR Monitor 2000-x8938-1119220';
const DEVICE_FRIENDLY_NAME = 'Mock HR 2000';

describe('get heart rate device connect error', () => {
  beforeEach(() => {
    Device.mockImplementation( () => {
      return {
        id: DEVICE_ID,
        name: DEVICE_NAME,
        friendlyName: DEVICE_FRIENDLY_NAME,
        isConnectable: false,
        connect: () => {
          return new Promise((resolve, reject) => {
            resolve(new Device());
          });
        },
        discoverAllServicesAndCharacteristics: () => {
          return new Promise((resolve, reject) => {
            reject({statusCode: StateCodes.OTHER, reason: 'general error discovering services on devicer'});
          });
        },
      };
    });

    BleManager.mockImplementation( () => {
      return {
        onStateChange: (listener, emitCurrentState) => {
          setTimeout(function() {
            listener('PoweredOn');
          }, 250);
          return ({
            remove: () => ( { } ),
          });
        },
      
        startDeviceScan: (UUIDs, options, listener) => {
          listener(null, new Device());
        },
        
        stopDeviceScan: () => {
        },


      };
    });
  });  
  
  test('should fail to discover the device', done => {
    function callback(device, error) {
      expect(device).toBe(null);
      expect(error).toMatchObject({"error": {"reason": "general error discovering services on devicer", "statusCode": StateCodes.OTHER}});
      done();
    }

    const hrm = new HeartRateMonitor();
    hrm.startHeartRateMonitor(DEVICE_ID, callback);
  })
})