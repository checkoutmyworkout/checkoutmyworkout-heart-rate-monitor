import { HeartRateMonitor, StateCodes } from "..";

import { BleManager, Device } from 'react-native-ble-plx';
jest.mock('react-native-ble-plx');

const DEVICE_ID = 'A3220F29-4A54-4530-B118-C3F0463A747D';
const DEVICE_NAME = 'Mock HR Monitor 2000-x8938-1119220';
const DEVICE_FRIENDLY_NAME = 'Mock HR 2000';

describe('get heart rate device connect error', () => {
  beforeEach(() => {
    Device.mockImplementation( () => {
      return {
        id: DEVICE_ID,
        name: DEVICE_NAME,
        friendlyName: DEVICE_FRIENDLY_NAME,
        isConnectable: false,
        connect: () => {
          return new Promise((resolve, reject) => {
            resolve(new Device());
          });
        },
        isConnected: () => {
          return true;
        },
        discoverAllServicesAndCharacteristics: () => {
          return new Promise((resolve, reject) => {
            resolve(new Device());
          });
        },
      };
    });

    BleManager.mockImplementation( () => {
      return {
        onStateChange: (listener, emitCurrentState) => {
          setTimeout(function() {
            listener('PoweredOn');
          }, 250);
          return ({
            remove: () => ( { } ),
          });
        },
      
        startDeviceScan: (UUIDs, options, listener) => {
          listener(null, new Device());
        },
        
        stopDeviceScan: () => {
        },

        monitorCharacteristicForDevice: (deviceId, serviceUUID, characteristicUUID, callback) => {
          callback({errorCode: StateCodes.OTHER, reason: 'unable to monitor characteristic'}, null);
        },

      };
    });
  });  
  
  test('should fail to monitor the characteristic', done => {
    function callback(heartRateEvent, error) {
      expect(heartRateEvent).toBe(null);
      expect(error).toMatchObject({"reason": "unable to monitor characteristic", "errorCode": StateCodes.OTHER});
      done();
    }

    const hrm = new HeartRateMonitor();
    hrm.startHeartRateMonitor(DEVICE_ID, callback);
  })

})