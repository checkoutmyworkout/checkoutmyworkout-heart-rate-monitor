import { HeartRateMonitor, StateCodes } from "..";

import { BleManager } from 'react-native-ble-plx';
jest.mock('react-native-ble-plx');

describe('device scan errors', () => {
  beforeEach(() => {
    jest.resetModules();

    BleManager.mockImplementation( () => {
      return {
        onStateChange: (listener, emitCurrentState) => {
          setTimeout(function() {
            listener('PoweredOn');
          }, 250);
          return ({
            remove: () => ( { } ),
          });
        },
      
        startDeviceScan: (UUIDs, options, listener) => {
          listener({errorCode: 'ScanStartFailed', reason: 'unable to start device scan'}, null);
        },
      };
    });
  });  
  
  test('device scan start error', done => {
    function callback(device, error) {
      expect(device).toBe(null);
      expect(error).toMatchObject({"scanCode": StateCodes.OTHER, "reason": "unable to start device scan"});
      done();
    }

    const hrm = new HeartRateMonitor();
    hrm.startHeartRateDevicesScan(callback);
  })

})