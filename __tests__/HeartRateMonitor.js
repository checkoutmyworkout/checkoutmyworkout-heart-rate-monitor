import { HeartRateMonitor, StateCodes } from "..";

import { BleManager, Device } from 'react-native-ble-plx';
jest.mock('react-native-ble-plx');

const DEVICE_ID = 'A3220F29-4A54-4530-B118-C3F0463A747D';
const DEVICE_NAME = 'Mock HR Monitor 2000-x8938-1119220';
const DEVICE_FRIENDLY_NAME = 'Mock HR 2000';

describe('heart rate monitor', () => {
  beforeEach(() => {
    Device.mockImplementation( () => {
      return {
        id: DEVICE_ID,
        name: DEVICE_NAME,
        friendlyName: DEVICE_FRIENDLY_NAME,
        isConnectable: false,
        connect: () => {
          return new Promise((resolve, reject) => {
            resolve(new Device());
          });
        },
        isConnected: () => {
          return true;
        },
        discoverAllServicesAndCharacteristics: () => {
          return new Promise((resolve, reject) => {
            resolve(new Device());
          });
        },
      };
    });

    BleManager.mockImplementation( () => {
      return {
        onStateChange: (listener, emitCurrentState) => {
          setTimeout(function() {
            listener('PoweredOn');
          }, 250);
          return ({
            remove: () => ( { } ),
          });
        },
      
        startDeviceScan: (UUIDs, options, listener) => {
          listener(null, new Device());
        },
        
        stopDeviceScan: () => {
        },

        monitorCharacteristicForDevice: (deviceId, serviceUUID, characteristicUUID, callback) => {
          callback(null,{value: 'EElJAw=='})
        },

        cancelDeviceConnection: (device) => {
          return new Promise((resolve, reject) => {
            resolve({
              ...device,
              isConnected: () => {
                return false;
              }
            });
          });
        }

      };
    });
  });  
  
  it('should scan and get devices', () => {
    const hrm = new HeartRateMonitor();
    return hrm.startHeartRateDevicesScan( (device, error) => {
      expect(error).toBe(undefined);
      expect(device.isConnectable).toEqual(false);
      expect(device.id).toEqual(DEVICE_ID);
      expect(device.name).toEqual(DEVICE_NAME);
      expect(device.friendlyName).toEqual(DEVICE_FRIENDLY_NAME);
    });
  })

  it('should stop devices scan', () => {
    const hrm = new HeartRateMonitor();
    return hrm.stopHeartRateDevicesScan()
      .then(result => {
        expect(result).toBe(true);
      })
  })

  it('should get a heart rate', () => {
    const hrm = new HeartRateMonitor();
    return hrm.startHeartRateMonitor(DEVICE_ID, (result) => {
      expect(result.heartRate).toEqual(73);
    });
  })

  it('should stop monitoring heart rate', () => {
    const hrm = new HeartRateMonitor();
    return hrm.stopHeartRateMonitor(DEVICE_ID)
      .then(result => {
        expect(result).toBe(true);
      })
  })
})